#
# salt-master
#
# Dockerized container for SaltStack master
#

FROM debian:stretch
MAINTAINER EagleRock <peter.marks@gmail.com>

# Standard OS Upgrade
RUN apt-get update && apt-get upgrade -y

# Install Salt
RUN apt-get install -y salt-master

# Volumes for important Salt directories
VOLUME ["/etc/salt/pki", "/var/cache/salt", "/var/logs/salt", "/etc/salt/master.d", "/srv"]

# Salt communication ports
EXPOSE 4505 4506

# Start Daemon
ENTRYPOINT ["/usr/bin/salt-master"]
