salt-master - Dockerized SaltStack Master Node
==============================================

This Docker image starts up a simple Docker container running a
SaltStack master node. The Dockerfile is loosely based off of the
soon/salt-master Dockerfile, but updated to a newer version of Salt
and using Debian as the base OS, rather than Ubuntu. Installing
salt-master on Debian is a much simpler process as well.

Ports
-----

Ports 4505 and 4506 are used for communication between the Salt Master
and the Salt Minions. These ports are exposed in the Docker image, and
will need to be exposed either by your Docker host or through your
orchestration suite (e.g. Docker Swarm or Kubernetes).

Volumes
-------

The following volumes are used in this install:

- /etc/salt/pki - Storage for master/minion keys, etc.
- /var/cache/salt - Salt cache directory
- /var/log/salt - Salt log directory
- /etc/salt/master.d - Master .conf file directory
- /srv - Data directory used to hold states, pillars, and formulas

These volumes can easily be managed by an orchestration engine like
Kubernetes. However, to run a single Docker container, it's recommended
to set up a busybox container with the necessary volumes, then use the
--volumes-from switch when creating the salt-master.

Setting up a Data Container
---------------------------

Here is an example of how to set up a data container. I use a directory
structure set up as follows:

```
salt
├── cache
├── logs
├── master.d
├── pki
└── srv
```

This allows all 5 directories to be persistent and easily managed from
the host. Here is an example salt-minion-data container based on using
this structure:

```
docker run -v /path/to/salt/pki:/etc/salt/pki \
           -v /path/to/salt/cache:/var/cache/salt \
           -v /path/to/salt/logs:/var/log/salt \
           -v /path/to/salt/master.d:/etc/salt/master.d \
           -v /path/to/salt/srv:/srv
           --name salt-master-data busybox true
```

Setting up the Salt Master
--------------------------

When setting up the salt-master on a Docker host with the salt-master-data
container's volumes, run the following:

```
docker run -d --name salt-master --volumes-from salt-master-data \
           -p 4505:4505 -p 4506:4506 eaglerock/salt-master
```

Note that the two ports need to be mapped to to the host, otherwise the
Master will not be able to communicate with its Minions.
